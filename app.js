// Configuracion Express
const express = require('express')
const app = express()
const port = 3000

// MOTOR de plantillas
app.set('view engine', 'ejs')
app.set('views', __dirname + '/views')

// USO de archivos estaticos
app.use(express.static(__dirname + "/public"))
// INDEX
app.get('/', (req, res) => {
    //console.log(__dirname)
    //res.send('Mi respuesta desde express')
    // Ya no se envian desde express, ahora se renderiza utilizando EJS
    res.render('index', {titulo: "Mi titulo dinamico"})
})
// EJEMPLO SERVICIOS
app.get('/servicios', (req, res) => {
    res.render('servicios', {
        titulo: "Titulo pagina",
        head: "Bienvenido a los servicios"
    })
})
// ERROR 404
app.use((req, res, netx) => {
    res.status(404).render('404', {
        titulo: "Mi pagina web"
    })
})

app.listen(port, () => {
    console.log('Servidor a su servicio')
})

// Ejemplo de solicitud GET desde navegador
// RUTAS de la aplicacion
// USO de archivos estaticos
app.use(express.static(__dirname + "/public"))
// Configurar pagina 404
// debe estar al inicio ya que si no encuentra una ruta carga 404.html

